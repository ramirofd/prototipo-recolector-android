# Primer prototipo de aplicación de recoleccion
Este trabajo es parte del proyecto de Investigación y Desarrollo presentado por 
el Laboratorio de Comunicaciones Digitales ante la Agencia Nacional de Promoción
 Científica y Tecnológica.
 
 El proyecto fue presentado en una convocatoria realizada por la agencia antes
 mencionada y dentro del marco de la pandemia por el COVID-19 y aprobado con 
 financiamiento para la realización y ejecución del mismo bajo la dirección del 
 Dr. Jorge Finochietto.
 
 ## Contenido del repositorio
 
 En este repositorio se encuentra la documentacion, diagramas de diseño y código
 de prototipos propuestos para relevar información y realizar pruebas de concepto.
