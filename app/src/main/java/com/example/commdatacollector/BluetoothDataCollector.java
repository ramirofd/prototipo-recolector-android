package com.example.commdatacollector;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class BluetoothDataCollector extends BroadcastReceiver {
    private ArrayList<BluetoothDataSample> bluetoothDataSamples;
    private MetadataCollector metadataCollector;
    private static BluetoothDataSample tempSample;
    private int period = 1;

    public BluetoothDataCollector(MetadataCollector metadataCollector) {
        super();
        this.bluetoothDataSamples = new ArrayList<>();
        this.metadataCollector = metadataCollector;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if(BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)){
            tempSample = new BluetoothDataSample(new Date(), metadataCollector);
            Log.i(this.getClass().getSimpleName(), action);
        } else if (BluetoothDevice.ACTION_FOUND.equals(action)) {
            BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            tempSample.add(new BluetoothScanResult(device));
            Log.i(this.getClass().getSimpleName(), new BluetoothScanResult(device).toCSV());
        } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)){
            bluetoothDataSamples.add(new BluetoothDataSample(tempSample));
            tempSample = null;
            startNextScan();
            Log.i(this.getClass().getSimpleName(), action);
        }
    }

    public String toCSV() {
        String csvData = "";
        for (BluetoothDataSample sample: bluetoothDataSamples) {
            csvData = csvData.concat(sample.toCSV());
        }
        bluetoothDataSamples.clear();
        return csvData;
    }

    private void startNextScan() {
        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                bluetoothAdapter.startDiscovery();
            }
        };
        timer.schedule(timerTask, period*60*1000);
    }
}
