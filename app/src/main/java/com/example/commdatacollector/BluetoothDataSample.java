package com.example.commdatacollector;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class BluetoothDataSample {

    private Date timestamp;
    private ArrayList<BluetoothScanResult> results;
    private String sensorsDataCSV;

    public BluetoothDataSample(Date date, MetadataCollector metadataCollector) {
        this.results = new ArrayList<>();
        sensorsDataCSV = metadataCollector.getMetadaSample();
        this.timestamp = date;
    }

    public BluetoothDataSample(BluetoothDataSample sample) {
        this.timestamp = sample.getTimestamp();
        this.results = sample.getResults();
        sensorsDataCSV = sample.getSensorsDataCSV();
    }

    public String getSensorsDataCSV() {
        return sensorsDataCSV;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public ArrayList<BluetoothScanResult> getResults() {
        return results;
    }

    public void add(BluetoothScanResult result) {
        results.add(result);
    }

    public String toCSV() {
        String csvData = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss",
                new Locale("en", "US"));
        String strTimestamp = sdf.format(timestamp);

        for( BluetoothScanResult blresult: results) {
            String data = String.format("%s,%s,%s\n", strTimestamp, blresult.toCSV(), sensorsDataCSV);
            csvData = csvData.concat(data);
        }
        return csvData;
    }
}
