package com.example.commdatacollector;

import android.bluetooth.le.ScanResult;

import java.util.Locale;

public class BluetoothLeScanResult {
    private BluetoothScanResult result;
    private long timestampNanoseconds;
    private int rssi;
    private int txPower;
    private int advertisingSid;
    private double periodicAdvertisingInterval;
    private int primaryPhysicalLayer;
    private int secondaryPhysicalLayer;


    public BluetoothLeScanResult(ScanResult result) {
        this.result = new BluetoothScanResult(result.getDevice());
        timestampNanoseconds = result.getTimestampNanos();
        rssi = result.getRssi();
        txPower = result.getTxPower();
        advertisingSid = result.getAdvertisingSid();
        periodicAdvertisingInterval = result.getPeriodicAdvertisingInterval()*1.25;
        primaryPhysicalLayer = result.getPrimaryPhy();
        secondaryPhysicalLayer = result.getSecondaryPhy();
    }

    public String toCSV() {
        String data = String.format(new Locale("en", "US"),
                "%s,%d,%d,%d,%d,%f,%d,%d\n",
                result.toCSV(), timestampNanoseconds, rssi, txPower, advertisingSid,
                periodicAdvertisingInterval, primaryPhysicalLayer, secondaryPhysicalLayer);

        return data;
    }
}
