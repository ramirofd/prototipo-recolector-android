package com.example.commdatacollector;

import android.bluetooth.BluetoothDevice;

import java.util.Locale;
// https://developer.android.com/reference/android/bluetooth/BluetoothDevice
// https://developer.android.com/reference/android/bluetooth/BluetoothClass.Device
// https://developer.android.com/reference/android/bluetooth/BluetoothClass.Device.Major
public class BluetoothScanResult {
    private String address;
    private int bluetoothClass;
    private int bluetoothMajorClass;
    private int bondState;
    private int type;

    public BluetoothScanResult(BluetoothDevice source) {
        address = source.getAddress();
        bluetoothClass = source.getBluetoothClass().getDeviceClass();
        bluetoothMajorClass = source.getBluetoothClass().getMajorDeviceClass();
        bondState = source.getBondState();
        type = source.getType();
    }

    public boolean equals(BluetoothScanResult obj) {
        return this.address.equals(obj.address);
    }

    public String toCSV() {
        String data = String.format(new Locale("en", "US"),
                "%s,%d,%d,%d,%d",
                        address, bluetoothClass, bluetoothMajorClass, bondState, type);

        return data;
    }
}
