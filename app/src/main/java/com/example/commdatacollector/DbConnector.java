package com.example.commdatacollector;

import android.content.Context;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class DbConnector {
    private File appDir;
    private File file;
    private Context context;

    public DbConnector() {
    }

    public boolean connect(Context context, String name) {

        appDir = context.getExternalFilesDir(null);

        if(appDir==null)
            return false;

        file = new File(appDir, name);
        return true;
    }

    public void disconnect() {
        appDir = null;
        file = null;
    }

    public boolean insertData(String data) {

        try {
            FileOutputStream outputStream = new FileOutputStream(file, true);
            OutputStreamWriter outputWriter = new OutputStreamWriter(outputStream);

            outputWriter.write(data);

            outputWriter.close();
            outputStream.close();

            return true;

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

}
