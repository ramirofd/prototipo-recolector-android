package com.example.commdatacollector;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.SensorManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class LoggerService extends Service {
    public static final String CHANNEL_ID = "LoggerServiceChannel";

    private Timer timer;
    private TimerTask timerTask;
    private DbConnector db;
    private WifiManager.WifiLock wifiLock;
    private PowerManager.WakeLock wakeLock;
    private MetadataCollector metadataCollector;
    private WifiDataCollector wifiDataCollector;
    private BluetoothDataCollector bluetoothDataCollector;
    private BluetoothLeScanner bluetoothLeScanner;
    private int period = 1; // Periodo en Minutos entre escrituras a archivo.
    private boolean enableWifi;
    private boolean enableBluetooth;
    private boolean enableBluetoothLE;


    public LoggerService() {
        db = new DbConnector();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        enableWifi = intent.getBooleanExtra("WIFI", false);
        enableBluetooth = intent.getBooleanExtra("BLUETOOTH", false);
        enableBluetoothLE = intent.getBooleanExtra("BLE", false);

        // Obtengo lock sobre el servicio de Energia para que no se duerma el proceso cuando el
        // telefono entra en standby.
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "commdatacollector:wakelock");
        wakeLock.acquire();

        // Instancio e inicializo el recolector de metadatos (por ahora solo sensores)
        SensorManager mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        metadataCollector = new MetadataCollector(mSensorManager);
        metadataCollector.registerSensors();

        if (enableWifi) {
            initializeWifiCollector();
        }
        if (enableBluetooth) {
            initializeBluetoothCollector();
        }
        if (enableBluetoothLE) {
            initializeBluetoothLECollector();
        }

        // Creo un canal de notificacion permanente para que el proceso del servicio informe al
        // usuario cuando esta realizando su trabajo.
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Comm Data Collector")
                .setContentText("Estamos juntando datos para cuidarte, gracias :D")
                .setSmallIcon(R.drawable.ic_stat_onesignal_default)
                .setContentIntent(pendingIntent)
                .build();

        // Inicializo el servicio en Foreground y tambien el timer para que se despierte
        // periodicamente a pedir datos y guardarlos.
        startForeground(1, notification);
        startTimer();

        return super.onStartCommand(intent, flags, startId);
    }

    // Device scan callback.
    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            Date now = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy",
                    new Locale("en", "US"));
            String bluetoothFileName = String.format("bluetooth-le-%s.csv", sdf.format(now));
            if(db.connect(getApplicationContext(), bluetoothFileName) && enableBluetoothLE) {
                db.insertData(new BluetoothLeScanResult(result).toCSV());
                db.disconnect();
            } else {
                db.disconnect();
            }
            Log.i(this.getClass().getSimpleName(), new BluetoothLeScanResult(result).toCSV());
        }
    };

    @Override
    public void onDestroy() {
        if (wifiLock != null && wifiLock.isHeld()) {
            wifiLock.release();
        }
        if (wakeLock != null && wakeLock.isHeld()) {
            wakeLock.release();
        }
        if(enableBluetooth)
            getApplicationContext().unregisterReceiver(bluetoothDataCollector);

        if(enableBluetoothLE)
            bluetoothLeScanner.stopScan(leScanCallback);

        if(enableWifi)
            getApplicationContext().unregisterReceiver(wifiDataCollector);

        metadataCollector.unregisterSensors();
        stopTimer();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void startTimer() {
        timer = new Timer();

        initializeTimerTask();

        timer.schedule(timerTask, 1000, period*60*1000); //
    }

    private void initializeWifiCollector() {
        // Obtengo lock sobre el servicio de WiFi para que no se corte cuando el telefono entra
        // en standby.
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        assert wm != null;
        wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL_HIGH_PERF, "commdatacollector:wifilock");
        wifiLock.acquire();

        // Registro un Filtro para que la aplicacion pueda capturar nuevos resultados de escaneo
        // de WiFi con el colector de datos.
        IntentFilter wifiIntentFilter = new IntentFilter();
        wifiIntentFilter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION);
        wifiDataCollector = new WifiDataCollector(metadataCollector);
        getApplicationContext().registerReceiver(wifiDataCollector, wifiIntentFilter);
    }

    private void initializeBluetoothCollector() {
        // Registro un Filtro para que la aplicacion pueda capturar nuevos resultados de escaneo
        // de Bluetooth con el colector de datos.
        IntentFilter bluetoothIntentFilter = new IntentFilter();
        bluetoothIntentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        bluetoothIntentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        bluetoothIntentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        bluetoothDataCollector = new BluetoothDataCollector(metadataCollector);
        getApplicationContext().registerReceiver(bluetoothDataCollector, bluetoothIntentFilter);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothAdapter.startDiscovery();
    }

    private void initializeBluetoothLECollector() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        bluetoothLeScanner.startScan(leScanCallback);
    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Date now = new Date();
                if(!logWifiData(now)) {
                    Log.e(this.getClass().getSimpleName(), "No se puedo escribir archivo.");
                }

                if(!logBluetoothData(now)) {
                    Log.e(this.getClass().getSimpleName(), "No se puedo escribir archivo.");
                }
            }
        };
    }

    private boolean logWifiData(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy",
                new Locale("en", "US"));
        String wifiFileName = String.format("wifi-%s.csv", sdf.format(date));
        if(db.connect(getApplicationContext(), wifiFileName) && enableWifi) {
            db.insertData(wifiDataCollector.toCSV());
            db.disconnect();
            return true;
        } else {
            db.disconnect();
            return false;
        }
    }

    private boolean logBluetoothData(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy",
                new Locale("en", "US"));
        String bluetoothFileName = String.format("bluetooth-%s.csv", sdf.format(date));
        if(db.connect(getApplicationContext(), bluetoothFileName) && enableBluetooth) {
            db.insertData(bluetoothDataCollector.toCSV());
            db.disconnect();
            return true;
        } else {
            db.disconnect();
            return false;
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Logger Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

}
