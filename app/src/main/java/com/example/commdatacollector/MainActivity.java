package com.example.commdatacollector;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Intent serviceIntent;
    private Button startBtn;
    private Button stopBtn;
    private Switch enableWifi;
    private Switch enableBluetooth;
    private Switch enableBluetoothLE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        enableWifi = findViewById(R.id.enable_wifi);
        enableBluetooth = findViewById(R.id.enable_bluetooth);
        enableBluetoothLE = findViewById(R.id.enable_ble);
        startBtn = findViewById(R.id.start_btn);
        stopBtn = findViewById(R.id.stop_btn);

        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter != null) {
            if (!bluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1234);
            }
        } else {
            enableBluetooth.setEnabled(false);
            enableBluetoothLE.setEnabled(false);
        }


        if (getApplicationContext().getSystemService(Context.WIFI_SERVICE) == null)
            enableWifi.setEnabled(false);


        startBtn.setEnabled(true);
        stopBtn.setEnabled(false);
    }

    public void startLoggerService(View view) {
        Toast.makeText(this, "Starting Logger Service", Toast.LENGTH_SHORT).show();

        serviceIntent = new Intent(this, LoggerService.class);
        serviceIntent.putExtra("WIFI", enableWifi.isChecked());
        serviceIntent.putExtra("BLUETOOTH", enableBluetooth.isChecked());
        serviceIntent.putExtra("BLE", enableBluetoothLE.isChecked());

        ContextCompat.startForegroundService(this, serviceIntent);
        startBtn.setEnabled(false);
        stopBtn.setEnabled(true);
    }

    public void stopLoggerService(View view) {
        if(serviceIntent != null) {
            Toast.makeText(this, "Stopping Logger Service", Toast.LENGTH_SHORT).show();
            stopService(serviceIntent);
            serviceIntent = null;
            startBtn.setEnabled(true);
            stopBtn.setEnabled(false);
        } else {
            Toast.makeText(this, "App is not Logging", Toast.LENGTH_SHORT).show();
        }
    }

}
