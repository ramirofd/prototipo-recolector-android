package com.example.commdatacollector;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class MetadataCollector implements SensorEventListener {

    private SensorManager sensorManager;
    private Map<String, float[]> valuesDictionary;
    private Sensor proximity;
    private Sensor ambientTemperature;
    private Sensor light;
    private Sensor pressure;
    private Sensor relativeHumidity;
    private Sensor stepDetector;

    public MetadataCollector(SensorManager sensorManager) {
        super();
        this.sensorManager = sensorManager;
        valuesDictionary = new HashMap<String, float[]>();
    }

    public void registerSensors() {
        proximity = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        ambientTemperature = sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
        light = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        pressure = sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        relativeHumidity = sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        stepDetector = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);

        valuesDictionary.put(Sensor.STRING_TYPE_PROXIMITY, null);
        if(proximity != null)
            sensorManager.registerListener(this, proximity, SensorManager.SENSOR_DELAY_NORMAL);

        valuesDictionary.put(Sensor.STRING_TYPE_AMBIENT_TEMPERATURE, null);
        if(ambientTemperature != null)
            sensorManager.registerListener(this, ambientTemperature, SensorManager.SENSOR_DELAY_NORMAL);

        valuesDictionary.put(Sensor.STRING_TYPE_LIGHT, null);
        if(light != null)
            sensorManager.registerListener(this, light, SensorManager.SENSOR_DELAY_NORMAL);

        valuesDictionary.put(Sensor.STRING_TYPE_PRESSURE, null);
        if(pressure != null)
            sensorManager.registerListener(this, pressure, SensorManager.SENSOR_DELAY_NORMAL);

        valuesDictionary.put(Sensor.STRING_TYPE_RELATIVE_HUMIDITY, null);
        if(relativeHumidity != null)
            sensorManager.registerListener(this, relativeHumidity, SensorManager.SENSOR_DELAY_NORMAL);

        valuesDictionary.put(Sensor.STRING_TYPE_STEP_DETECTOR, null);
        if(stepDetector != null)
            sensorManager.registerListener(this, stepDetector, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void unregisterSensors() {
        sensorManager.unregisterListener(this);
    }

    public String getMetadaSample() {
        return String.format("%s", getSensorsSample());
    }
    
    private String getSensorsSample() {
        String csvData = "";
        for (Map.Entry<String, float[]> entry : valuesDictionary.entrySet()) {
            String[] splittedKey = entry.getKey().split("\\.");
            String key = splittedKey[splittedKey.length-1];
            if(entry.getValue() != null) {
                for (int i = 0; i < entry.getValue().length; i++)
                    csvData = csvData.concat(String.format(new Locale("en", "US"),
                            "%s-%d:%s,", key, i, Float.toString(entry.getValue()[i]))
                    );
            } else {
                csvData = csvData.concat(String.format("%s:NULL,", key));
            }
        }
        csvData = csvData.substring(0, csvData.length() - 1);
        return csvData;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        Log.i(event.sensor.getStringType(), String.format("%f", event.values[0]));
        valuesDictionary.put(event.sensor.getStringType(), event.values);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
