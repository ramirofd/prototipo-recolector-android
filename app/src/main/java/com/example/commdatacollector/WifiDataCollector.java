package com.example.commdatacollector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;

import java.util.ArrayList;
import java.util.Date;

public class WifiDataCollector extends BroadcastReceiver {

    private ArrayList<WifiDataSample> wifiDataSamples;
    private MetadataCollector metadataCollector;

    public WifiDataCollector(MetadataCollector metadataCollector) {
        super();
        this.wifiDataSamples = new ArrayList<>();
        this.metadataCollector = metadataCollector;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean success = intent.getBooleanExtra(
                WifiManager.EXTRA_RESULTS_UPDATED, false);
        if (success) {
            this.processScanResults(context);
        }
    }

    public String toCSV() {
        String csvData = "";
        for (WifiDataSample sample: wifiDataSamples) {
            csvData = csvData.concat(sample.toCSV());
        }
        wifiDataSamples.clear();
        return csvData;
    }

    private void processScanResults(Context context){
        wifiDataSamples.add(new WifiDataSample(context, new Date(), metadataCollector));
    }
}
