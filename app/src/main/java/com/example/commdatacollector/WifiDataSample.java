package com.example.commdatacollector;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class WifiDataSample {

    private Date timestamp;
    private ArrayList<WifiScanResult> results;
    private String sensorsDataCSV;

    public WifiDataSample(Context context, Date date, MetadataCollector metadataCollector) {
        WifiManager wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        assert wm != null;
        ArrayList<ScanResult> results = (ArrayList<ScanResult>) wm.getScanResults();
        sensorsDataCSV = metadataCollector.getMetadaSample();
        this.results = new ArrayList<>();
        for (ScanResult scanResult: results) {
            this.results.add(new WifiScanResult(scanResult));
        }
        this.timestamp = date;

    }


    public String toCSV() {
        String csvData = "";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy'T'HH:mm:ss",
                new Locale("en", "US"));
        String strTimestamp = sdf.format(timestamp);
        for (WifiScanResult wresult : results) {
            String data = String.format("%s,%s,%s\n", strTimestamp, wresult.toCSV(), sensorsDataCSV);
            csvData = csvData.concat(data);
        }
        return csvData;
    }

}
