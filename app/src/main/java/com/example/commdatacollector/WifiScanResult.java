package com.example.commdatacollector;

import android.net.wifi.ScanResult;

import java.util.Locale;

public class WifiScanResult{

    private String BSSID;
    private String SSID;
    private String capabilities;
    private int centerFreq0;
    private int centerFreq1;
    private int channelWidth;
    private int frequency;
    private int level;
    private int isPasspoint;
    private String passpointOperatorName;
    private String passpointVenueName;
    // https://developer.android.com/reference/android/net/wifi/ScanResult
    public WifiScanResult(ScanResult source){
        BSSID = source.BSSID;
        SSID = source.SSID;
        capabilities = source.capabilities;
        centerFreq0 = source.centerFreq0;
        centerFreq1 = source.centerFreq1;
        channelWidth = source.channelWidth;
        frequency = source.frequency;
        level = source.level;
        if(source.isPasspointNetwork()) {
            isPasspoint = 1;
            passpointOperatorName = source.operatorFriendlyName.toString();
            passpointVenueName = source.venueName.toString();
        } else {
            isPasspoint = 0;
            passpointOperatorName = "NULL";
            passpointVenueName = "NULL";
        }
    }

    public boolean equals(WifiScanResult obj){
        return this.BSSID.equals(obj.BSSID);
    }

    public String toCSV() {
        String data = String.format(new Locale("en", "US"),
                            "%s,%s,%d,%d,%d,%d,%d,%d,%s,%s",
                                    BSSID, SSID, centerFreq0, centerFreq1, frequency, channelWidth,
                level,
                                    isPasspoint, passpointOperatorName, passpointVenueName);
        return data;
    }
}
